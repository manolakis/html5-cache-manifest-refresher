(function(){
	'use strict';

	if ('applicationCache' in window) { // check if browser supports application cache
		window.addEventListener('load', function(){

			var appcache = window.applicationCache;
			appcache.addEventListener('updateready', function(){
				if (appcache.status === appcache.UPDATEREADY) {
					appcache.swapCache();

					if (confirm('Existe una nueva versión de la aplicación. ¿Recargar?')){
						window.location.reload();
					}
				}
			}, false);

		}, false);
	}
	
})();